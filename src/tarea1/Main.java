/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package tarea1;

/**
 *
 * @author PC
 */
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int S = 0, A = 0, M = 0, I = 0;
        boolean repeat;
        do {
            repeat = false;
            try {
                System.out.print("Enter first integer: ");
                S = sc.nextInt();

                System.out.print("Enter second integer: ");
                A = sc.nextInt();

                System.out.print("Enter third integer: ");
                M = sc.nextInt();

                System.out.print("Enter fourth integer: ");
                I = sc.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Invalid value" + e.toString());
                sc.nextLine();
                repeat = true;
            }
        } while (repeat);
        System.out.println("int inserted -> " + S);
        System.out.println("int inserted -> " + A);
        System.out.println("int inserted -> " + M);
        System.out.println("int inserted -> " + I);
    }

}
